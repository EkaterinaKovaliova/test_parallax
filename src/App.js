import TestParallax from './modules/TestParallax';

function App() {
  return (
    <>
      <TestParallax />
    </>
  );
}

export default App;
