import React from 'react';
import ReactDOM from 'react-dom';
import { ParallaxProvider } from 'react-scroll-parallax';

import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
   <ParallaxProvider><App /></ParallaxProvider>
  ,
  document.getElementById('root')
);

reportWebVitals();
