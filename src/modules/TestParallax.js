import { Parallax } from 'react-scroll-parallax';
import { Timon } from '../assets';

import './TestParallax.css';

function TestParallax() {
  return (
    <>
      <div className='top-box'>
        <h1 className='title'>Now you see the magic</h1>
        <Parallax className='parallax-top-box' y={[-50, 30]}>
          <div className='parallax-top-content'>1</div>
        </Parallax>
      </div>
      <div className='bottom-block'>
        <Parallax className='parallax-bottom-box' x={[-80, 100]}>
          <img className='parallax-bottom-content' src={Timon} alt='Timon' />
        </Parallax>
      </div>
    </>
  );
}

export default TestParallax;
